# StatsForBots

Is a simple bot that helps you keep track of your overwatch statistics. Information is retreived from https://overbuff.com.

# Images (Examples)

[![!activity](https://imgur.com/oCmzzKfm.png)](https://imgur.com/oCmzzKf.png)
[![!activity_with_heroes](https://i.imgur.com/n6D8Rafm.png)](https://i.imgur.com/n6D8Raf.png)
[![!competitive_hero Widowmaker](https://imgur.com/PxPhrYom.png)](https://imgur.com/PxPhrYo.png)

# Can I use it?
Yes, but it's very much so in alpha state. Meaning it's unstable, contains bugs and has issues ;). It's also running on a single core/512mb vps from DigitalOcean, so in case the the usage goes sky high (although I don't believe many will use this) I might limit usage. 

# How can I make this bot join my channel? 
Simple, click on this link:

[Let StatsForBots join your channel!](https://discordapp.com/oauth2/authorize?&client_id=518428128184631296&scope=bot&permissions=0).

# Features / Commands
- `!register BattleTag#1234` : Use your Battle.Net battletag to register yourself on the bot. It will keep track of your statistics for you! 
- `!register BattleTag#1234 --private` : Use your Battle.Net battletag to register yourself on the bot. It will keep track of your statistics for you and send you the updates via PM's! 
- `!unregister BattleTag#1234` : You can leave if you want...
- `!activity BattleTag#1234` : Get your latest activity manually
- `!activity_with_heroes BattleTag#1234` : Get your latest activity manually, but show played heroes as well (if a lot, it can spam quit a bit)
- `!competitive_hero "HeroName" BattleTag#1234` : Get specific hero statistics (competitive). Be aware, it uses Overbuffs Hero Name scheme and requires capitals if they are there.
- `!help` : In case you need help.


### Tech
StatsForBots uses the following tech:
* [OverBuffStats.NET]() - Not available yet, WIP... . 
* [Discord.NET](https://github.com/RogueException/Discord.Net) - Awesome and easy to use Discord.Net wrapper for Discord.
* [Json.NET](https://www.newtonsoft.com/json) - Well... obviously.
* [HtmlAgilityPack](https://html-agility-pack.net) - For scraping https://overbuff.com


### Changelog
 - v0.0.4
    - Added support for PM.
    - Fixed some bugs with incomplete profiles/private profiles.
    - Save registered battletags so no re-registering is needed after the bot restarts.

### Todos

 - Actually make readable and maintainable code (DPI + Inversion of Control)
 - Logging
 - More customizability
 - Some interface to configure the bot for your specific needs ;).
 - Time

License
----

MIT

**Free Software, Hell Yeah!**

