﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordOverBuffBot.Models
{
    class RecentActivity
    {
        public string Type { get; set; } = "0";
        public string SkillRating { get; set; } = "0";
        public string Draw { get; set; } = "0";
        public string Lost { get; set; } = "0";
        public string Won { get; set; } = "0";
        public string Date { get; set; } = "0";
        public string DateMS { get; set; } = "0";
        public Dictionary<string, string> ActivityStats { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> HeroesPlayed { get; set; } = new Dictionary<string, string>();
    }
}
