﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordOverBuffBot.Models
{
    public class BattleTagLastUpdate
    {
        public string tag { get; set; } = "";
        public string time { get; set; } = "0";
    }
}
