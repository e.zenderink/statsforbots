﻿using DiscordOverBuffBot.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordOverBuffBot.Heroes
{
    class CompetitiveProfile
    {
        public bool PublicProfile { get; set; } = true;
        public string UserIcon { get; set; } = "0";
        public string UserName { get; set; } = "0";
        public string UserID { get; set; } = "0";
        public string SkillRank { get; set; } = "0";
        public string SkillRating { get; set; } = "0";
        public string OnFire { get; set; } = "0";
        public string Won{ get; set; } = "0";
        public string Lost { get; set; } = "0";
        public string Draws { get; set; } = "0";
        public string WinRate { get; set; } = "0";
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> HeroesPlayed { get; set; } = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
        public Dictionary<string, RecentActivity> RecentActivity { get; set; } = new Dictionary<string, RecentActivity>();
    }

   
}
