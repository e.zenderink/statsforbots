﻿using DiscordOverBuffBot.Heroes;
using DiscordOverBuffBot.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DiscordOverBuffBot.HtmlParser
{
    class ParseCompetitiveProfile
    {
        private readonly HtmlWeb _webParers;
        private CompetitiveProfile _profile;
        private readonly Dictionary<string,string> _competitiveProfileInfoQuery = new Dictionary<string, string>()
        {
            { "CHECK_AVAILABLE", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/span"},
            { "NAME", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[1]/div/div[2]/h1/text()" },
            { "ICON", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[1]/div/div[1]/a/div/img/@src" },
            { "ID" ,  "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[1]/div/div[2]/h1/small" },
            { "SKILLRANK", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[1]/dd/span" },
            { "SKILLRATING", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[2]/dd/span/span" },
            { "ONFIRE", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[3]/dd" },
            { "WON","/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[4]/dd/span[1]" },
            { "DRAW", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[4]/dd/span[5]" },
            { "LOST", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[4]/dd/span[3]" },
            { "WINRATE", "/html/body/div[1]/div[3]/div/div[2]/div[1]/div[2]/div[1]/dl[5]/dd" }
        };

        private readonly Dictionary<string, string> _competitiveHeroInfoQuery = new Dictionary<string, string>()
        {
                            ///html/body/div[1]/div[3]/div/div[3]/div[1]/section/article/div[2]/div[12]/div[1]/div[1]/div[2]/a
            { "LIST_HEROES", "/html/body/div[1]/div[3]/div[1]/div[3]/div/section/article/div[2]" }, //contains html, further parsing needed!
            { "HERO_ICON", "div[1]/div[1]/div[1]/a/div[1]/img/@src" }, //contains image tag, strip url!
            { "HERO_NAME", "div[1]/div[1]/div[2]/a" },
            { "HERO_URL", "div[1]/div[1]/div[2]/a/@href" },
            { "LIST_PRIMARY_DATA", "div[3]/div[1]"},
            { "LIST_SECONDARY_DATA", "div[3]/div[2]"}
        };

        private readonly Dictionary<string, string> _competitiveLatestActivityInfoQuery = new Dictionary<string, string>()
        {                        ///html/body/div[1]/div[3]/div/div[3]/div[2]/div[4]/div/section/article/div
                         
             { "LIST_ACTIVITIES", "/html/body/div[1]/div[3]/div/div[3]/div[2]/div[4]/div/section/article/div" },
                      //div[1]/div[1]/div/div[1]/div[1]
             { "TYPE", "div[1]/div[1]/div/div[1]/div[1]" },
             { "DATE","div[1]/div[1]/div/div[1]/div[2]/span/@data-time" },
             { "WON", "div[1]/div[1]/div/div[2]/div[1]/span[contains(@class, 'color-stat-win')]" },
             { "LOST", "div[1]/div[1]/div/div[2]/div[1]/span[contains(@class, 'color-stat-loss')]" },
             { "DRAW", "div[1]/div[1]/div/div[2]/div[1]/span/span[contains(@class, 'color-stat-draw')]" },
             { "SKILLRATING", "div[1]/div[2]/div/div[2]/div[1]/text()" },
             { "LIST_ACTIVITY_STATS", "div[1]/div[3]/div" },
             { "LIST_HEROES_PLAYED", "div[1]/div[4]/div/div" }, //html, list with hero images, need to iterate!
        };

        public ParseCompetitiveProfile()
        {
            _webParers = new HtmlWeb();
        }

        public CompetitiveProfile ParseProfile(string battleTag)
        {
            _profile = new CompetitiveProfile();
            string url = "https://www.overbuff.com/players/pc/" + battleTag + "?mode=competitive";
            HtmlDocument page = _webParers.Load(url);

            HtmlNode node = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["CHECK_AVAILABLE"]);

            if (node != null)
            {
                if (node.InnerText.Trim() == "Cannot Update Stats")
                {
                    _profile.PublicProfile = false;
                }
                else
                {
                    ParseGeneralInformation(page);
                    ParseHeroes(page);
                    ParseRecentActivity(page);
                }
            }
            else
            {
                ParseGeneralInformation(page);
                ParseHeroes(page);
                ParseRecentActivity(page);
            }


            return _profile; 
        }

        public async Task<bool> CheckRefresh(string battleTag)
        {
            string url = "https://www.overbuff.com/players/pc/" + battleTag + "/refresh";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Referer", "https://www.overbuff.com/players/pc/" + battleTag + "?mode=competitive");
            HttpResponseMessage response = await client.PostAsync(url, new StringContent(" "));

            if (response.IsSuccessStatusCode)
            {
                string response_content = await response.Content.ReadAsStringAsync();
                if (response_content.Trim() != "{\"updated\":false}")
                {
                    Console.WriteLine(response_content);
                    Console.WriteLine("There should be an update yeet.");
                    return true;
                }
                else
                {
                    Console.WriteLine("There is no update yeet.");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("The request feeled yoinked..");
                return false;
            }

        }

        public void ParseGeneralInformation(HtmlDocument page)
        {
            HtmlNode playerName = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["NAME"]);
            HtmlNode playerIcon = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["ICON"]);
            HtmlNode playerId = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["ID"]);
            HtmlNode playerSkillRank = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["SKILLRANK"]);
            HtmlNode playerSkillRating = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["SKILLRATING"]);
            HtmlNode playerOnFire = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["ONFIRE"]);
            HtmlNode playerWon = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["WON"]);
            HtmlNode playerLost = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["LOST"]);
            HtmlNode playerDraws = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["DRAW"]);
            HtmlNode playerWinRate = page.DocumentNode.SelectSingleNode(_competitiveProfileInfoQuery["WINRATE"]);

            if (playerName != null)
            {
                _profile.UserName = playerName.InnerText.Trim();
            }

            if (playerIcon != null)
            {
                _profile.UserIcon = playerIcon.Attributes["src"].Value;
            }

            if (playerId != null)
            {
                _profile.UserID = playerId.InnerText.Trim();
            }

            if (playerSkillRank != null)
            {
                _profile.SkillRank = playerSkillRank.InnerText.Trim();
            }

            if (playerSkillRating != null)
            {
                _profile.SkillRating = playerSkillRating.InnerText.Trim();
            }

            if (playerOnFire != null)
            {
                _profile.OnFire = playerOnFire.InnerText.Trim();
            }

            if (playerWon != null)
            {
                _profile.Won = playerWon.InnerText.Trim();
            }

            if (playerDraws != null)
            {
                _profile.Draws = playerDraws.InnerText.Trim();
            }

            if (playerLost != null)
            {
                _profile.Lost = playerLost.InnerText.Trim();
            }

            if (playerWinRate != null)
            {
                _profile.WinRate = playerWinRate.InnerText.Trim();
            }

            Console.WriteLine("DONE PARSING GENERAL INFO" );
        }

        public void ParseHeroes(HtmlDocument page)
        {
            HtmlNodeCollection heroes = page.DocumentNode.SelectSingleNode(_competitiveHeroInfoQuery["LIST_HEROES"]).ChildNodes;
            Dictionary<string, Dictionary<string, Dictionary<string, string>>> herolist = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
            if (heroes != null)
            {

                foreach (var hero in heroes)
                {
                    if (hero.NodeType == HtmlNodeType.Element)
                    {
                        Dictionary<string, Dictionary<string, string>> statsandinfo = new Dictionary<string, Dictionary<string, string>>();
                        HtmlNode heroName = hero.SelectSingleNode(_competitiveHeroInfoQuery["HERO_NAME"]);
                        HtmlNode heroIcon = hero.SelectSingleNode(_competitiveHeroInfoQuery["HERO_ICON"]);
                        HtmlNode heroLink = hero.SelectSingleNode(_competitiveHeroInfoQuery["HERO_URL"]);
                        HtmlNodeCollection primaryStats = hero.SelectSingleNode(_competitiveHeroInfoQuery["LIST_PRIMARY_DATA"]).ChildNodes;
                        HtmlNodeCollection secondaryStats = hero.SelectSingleNode(_competitiveHeroInfoQuery["LIST_SECONDARY_DATA"]).ChildNodes;

                        
                        Dictionary<string, string> heroPrimaryStats = new Dictionary<string, string>();
                        if (primaryStats != null)
                        {
                            foreach (var primary_stat in primaryStats)
                            {
                                if (primary_stat.NodeType == HtmlNodeType.Element)
                                {
                                    HtmlNode field = primary_stat.SelectSingleNode("div[3]/text()");
                                    HtmlNode value = primary_stat.SelectSingleNode("div[1]/text()");
                                    if (field != null && value != null)
                                    {
                                        heroPrimaryStats.Add(field.InnerText.Trim(), value.InnerText.Trim());
                                    }
                                }
                            }
                        }
                       

                        Dictionary<string, string> heroSecondaryStats = new Dictionary<string, string>();
                        if (secondaryStats != null)
                        {
                            foreach (var secondary_stat in secondaryStats)
                            {
                                if (secondary_stat.NodeType == HtmlNodeType.Element)
                                {
                                    HtmlNode field = secondary_stat.SelectSingleNode("div[3]/text()");
                                    HtmlNode value = secondary_stat.SelectSingleNode("div[1]/text()");

                                    if (field != null && value != null)
                                    {
                                        heroSecondaryStats.Add(field.InnerText.Trim(), value.InnerText.Trim());
                                    }
                                }
                            }
                        }

                        Dictionary<string, string> heroInfo = new Dictionary<string, string>();
                        if (heroName != null)
                        {
                            heroInfo.Add("hero_name", heroName.InnerText.Trim());
                        }
                        if (heroIcon != null)
                        {
                            heroInfo.Add("hero_icon", "https://www.overbuff.com" + heroIcon.Attributes["src"].Value);
                        }
                        if (heroLink != null)
                        {
                            heroInfo.Add("hero_url", "https://www.overbuff.com" + heroLink.Attributes["href"].Value);
                        }

                        statsandinfo.Add("hero_info", heroInfo);
                        statsandinfo.Add("primary_statistics", heroPrimaryStats);
                        statsandinfo.Add("secondary_statistics", heroSecondaryStats);

                        if (!herolist.ContainsKey(heroName.InnerText.Trim()))
                        {
                            herolist.Add(heroName.InnerText.Trim(), statsandinfo);
                        }
                    }
                }

                _profile.HeroesPlayed = herolist;
            }
        }

        public void ParseRecentActivity(HtmlDocument page)
        {

            HtmlNodeCollection activities = page.DocumentNode.SelectNodes(_competitiveLatestActivityInfoQuery["LIST_ACTIVITIES"]);

            Dictionary<string, RecentActivity> recentActivity = new Dictionary<string, RecentActivity>();
            if (activities != null)
            {
                foreach (var activity in activities)
                {

                    HtmlNode type = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["TYPE"]);
                    HtmlNode date = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["DATE"]);
                    HtmlNode won = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["WON"]);
                    HtmlNode lost = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["LOST"]);
                    HtmlNode draw = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["DRAW"]);
                    HtmlNode skillrating = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["SKILLRATING"]);

                    HtmlNodeCollection activityStatNodes = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["LIST_ACTIVITY_STATS"]).ChildNodes;
                    HtmlNodeCollection heroesPlayedNodes = activity.SelectSingleNode(_competitiveLatestActivityInfoQuery["LIST_HEROES_PLAYED"]).ChildNodes;

                    Dictionary<string, string> activityStats = new Dictionary<string, string>();

                    foreach (var actitityStat in activityStatNodes)
                    {
                        HtmlNode field = actitityStat.SelectSingleNode("div[3]/text()");
                        HtmlNode value = actitityStat.SelectSingleNode("div[1]/text()");

                        Console.WriteLine(field.InnerText + " " + value.InnerText);
                        activityStats.Add(field.InnerText, value.InnerText);
                    }


                    Dictionary<string, string> heroesPlayed = new Dictionary<string, string>();

                    foreach (var hero in heroesPlayedNodes)
                    {
                        HtmlNode field = hero.SelectSingleNode("a/img/@alt");
                        heroesPlayed.Add(field.Attributes["alt"].Value, "https://www.overbuff.com" + field.Attributes["src"].Value);
                    }

                    TimeSpan timeSpan = TimeSpan.FromSeconds(long.Parse(date.Attributes["data-time"].Value));
                    RecentActivity recentActivityClass = new RecentActivity();

                    recentActivityClass.Type = type.InnerText;
                    if (type.InnerText != "Quick Play")
                    {
                        recentActivityClass.Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).Add(timeSpan).ToString("yyyy-MM-dd HH:mm:ss");
                        recentActivityClass.DateMS = date.Attributes["data-time"].Value;
                        if (won != null)
                        {
                            recentActivityClass.Won = won.InnerText.Trim();
                        }
                        if (draw != null)
                        {
                            recentActivityClass.Draw = draw.InnerText.Trim();
                        }
                        if (lost != null)
                        {
                            recentActivityClass.Lost = lost.InnerText.Trim();
                        }
                        recentActivityClass.SkillRating = skillrating.InnerText.Trim();
                        recentActivityClass.ActivityStats = activityStats;
                        recentActivityClass.HeroesPlayed = heroesPlayed;
                    }
                    else
                    {
                        if (won != null)
                        {
                            Console.WriteLine("QP-WON: " + won.InnerText.Trim());
                            recentActivityClass.Won = won.InnerText.Trim();
                        }
                        if (draw != null)
                        {
                            recentActivityClass.Draw = draw.InnerText.Trim();
                        }
                        if (lost != null)
                        {
                            recentActivityClass.Lost = lost.InnerText.Trim();
                        }
                        recentActivityClass.Date = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).Add(timeSpan).ToString("yyyy-MM-dd HH:mm:ss");

                        Console.WriteLine("QP-WON: " + recentActivityClass.Date);
                        recentActivityClass.DateMS = date.Attributes["data-time"].Value;
                        recentActivityClass.ActivityStats = activityStats;
                        recentActivityClass.HeroesPlayed = heroesPlayed;
                    }

                 

                    recentActivity.Add((new DateTime(1970, 1, 1, 0, 0, 0, 0)).Add(timeSpan).ToString("yyyy-MM-dd HH:mm:ss"), recentActivityClass);

                }
                _profile.RecentActivity = recentActivity;
            }
           
        }
        
    }
}
