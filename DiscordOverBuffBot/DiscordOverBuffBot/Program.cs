﻿using Discord;
using Discord.WebSocket;
using DiscordOverBuffBot.Heroes;
using DiscordOverBuffBot.HtmlParser;
using DiscordOverBuffBot.Models;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiscordOverBuffBot
{
    class Program
    {

        private static DiscordSocketClient discordSocketClient = new DiscordSocketClient();

        private static Dictionary<ulong, Dictionary<ulong,List<BattleTagLastUpdate>>> BattleTags = new Dictionary<ulong, Dictionary<ulong, List<BattleTagLastUpdate>>>();

        public static void Main(string[] args)
        {
            Console.WriteLine("Starting BOT Server!");
            StartConnection();
            Console.ReadLine();
            
        }

        private async static Task StartConnection()
        {

            discordSocketClient.Log += Log;
            discordSocketClient.MessageReceived += MessageReceived;
            Console.WriteLine("Starting Server");
            await discordSocketClient.LoginAsync(TokenType.Bot, "NTE4NDI4MTI4MTg0NjMxMjk2.DuQrQQ.0qXg0QK3AuV0y4wK7kqA4887_MY");
            await discordSocketClient.StartAsync();

            Thread t = new Thread(() => { RunTracker(); });
            t.Start();
            Console.WriteLine("Server Running");
            await Task.Delay(-1);
        }

        private static async void RunTracker()
        {
            while (true)
            {

                foreach (KeyValuePair<ulong, Dictionary<ulong, List<BattleTagLastUpdate>>> keyValuePair1 in BattleTags)
                {
                    foreach (KeyValuePair<ulong, List<BattleTagLastUpdate>> keyValuePair2 in keyValuePair1.Value)
                    {
                        int index = 0;
                        foreach (BattleTagLastUpdate battleTagLastUpdate in keyValuePair2.Value) {
                            string battleTag = battleTagLastUpdate.tag.Replace('#', '-');
                            ParseCompetitiveProfile parser = new ParseCompetitiveProfile();

                            if (await parser.CheckRefresh(battleTag))
                            {
                                CompetitiveProfile competitiveProfile = parser.ParseProfile(battleTag);

                                Console.WriteLine("Update for player: " + battleTagLastUpdate.tag);
                                try
                                {

                                    if (long.Parse(competitiveProfile.RecentActivity.First().Value.DateMS) > long.Parse(battleTagLastUpdate.time))
                                    {
                                        Console.WriteLine("There is a new update for player: " + battleTagLastUpdate.tag);
                                        Embed post = GenerateRecentActivity(competitiveProfile).Build();
                                        BattleTags[keyValuePair1.Key][keyValuePair2.Key][index].time = competitiveProfile.RecentActivity.First().Value.DateMS;
                                        await discordSocketClient.GetGuild(keyValuePair1.Key).GetTextChannel(keyValuePair2.Key).SendMessageAsync("", false, post);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                            else
                            {
                                Console.WriteLine("No update for player: " + battleTagLastUpdate.tag);
                            }
                            
                            index++;
                        }
                    }                 
                }
                Thread.Sleep(30000);
            }
        }


        private static async Task MessageReceived(SocketMessage message)
        {
            Console.WriteLine("Received Message From Channel: " + message.Channel.Name);
            Console.WriteLine("Received Message From User: " + message.Author.Username);
            Console.WriteLine("Received Message From Content: " + message.Content.ToString());

            if (!BattleTags.ContainsKey(((SocketGuildChannel)message.Channel).Guild.Id))
            {
                BattleTags.Add(((SocketGuildChannel)message.Channel).Guild.Id, new Dictionary<ulong, List<BattleTagLastUpdate>>());
            }

            string command = message.Content;

            string[] words = new string[5];
            if (command.Contains(' '))
            {
                words = command.Split(' ');
                command = words[0];
            }

            switch(command)
            {

                case "!competitive_hero":

                    string hero = words[1];
                    string battleTag1 = words[2];


                    await message.Channel.SendMessageAsync("Retreiving hero information for hero: " + hero + " for player: " + battleTag1);

                    string urlReadyBattleTag1 = battleTag1.Replace('#', '-');
                    ParseCompetitiveProfile parser1 = new ParseCompetitiveProfile();
                    CompetitiveProfile competitiveProfile1 = parser1.ParseProfile(urlReadyBattleTag1);

                    if (competitiveProfile1.PublicProfile)
                    {
                        EmbedBuilder heropost = GenerateHeroPost(competitiveProfile1, hero);
                        Console.WriteLine("Finished Generating hero post: " + hero);
                        if (heropost != null)
                        {
                            try
                            {
                                Console.WriteLine("Posting hero post: " + heropost.Title);
                                Embed embed = heropost.Build();
                                await message.Channel.SendMessageAsync("", false, embed);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.ToString());
                            }
                        }
                        else
                        {

                            Console.WriteLine("Hero post returned NULL");
                        }
                    }
                    break;
                case "!help":

                    EmbedBuilder generalInfo = new EmbedBuilder();
                    generalInfo.WithTitle("Bot Explanation!");
                    generalInfo.WithDescription("Bot Version: v0.0.3-alpha. \n \n Bot Author: MrRare#8259. \n \n **This bot is still in Alpha, it may contain many [bugs](https://imgs.xkcd.com/comics/new_bug.png) and [issues](https://imgs.xkcd.com/comics/fixing_problems.png). Just give me a shot and I will look at it. Source code: [GitLab](https://gitlab.com/e.zenderink/statsforbots/tree/master). \n \n This bot is designed unlike other overwatch bots to automatically check the latest changes on your Overbuff Profile page. \n \n You can only check your competitive profile (for now). \n \n You can register your battletag to automatically log when a change happen.\n You can check the latest activity manually, with and without hero information. It's all up to you! \n\n **Your Overwatch profile needs to be PUBLIC!**  \n");
                  
                    EmbedFieldBuilder registerautoupdate = new EmbedFieldBuilder();
                    registerautoupdate.WithName("!register");
                    registerautoupdate.WithValue("Registers your battletag to check for updates! \n Usage: ``!registers battletag#1234`` \n");
                    EmbedFieldBuilder unregisterautoupdate = new EmbedFieldBuilder();
                    unregisterautoupdate.WithName("!unregister");
                    unregisterautoupdate.WithValue("Stops checking for auto_updates! \n Usage: ``!unregister battletag#1234`` \n");
                    EmbedFieldBuilder activity = new EmbedFieldBuilder();
                    activity.WithName("!activity");
                    activity.WithValue("Check your latest activity! \n Usage: ``!activity battletag#1234`` \n");
                    EmbedFieldBuilder activitywiththeroes = new EmbedFieldBuilder();
                    activitywiththeroes.WithName("!activity_with_heroes");
                    activitywiththeroes.WithValue("Check your latest activity and show each hero that you played! \n Usage: ``!activity_with_heroes battletag#1234`` \n");
                    EmbedFieldBuilder competitive_hero = new EmbedFieldBuilder();
                    competitive_hero.WithName("!competitive_hero");
                    competitive_hero.WithValue("Check competitive statistics for your specified hero! \n \n **Heroname is capital specific, uses [Overbuff](https://www.overbuff.com/heroes) names for heroes. ** \n \n Usage: ``!competitive_hero HeroName battletag#1234``");
                    /*EmbedFieldBuilder competitiveshort = new EmbedFieldBuilder();
                    competitiveshort.WithName("!competitive_short");
                    competitiveshort.WithValue("Get the most important information from your competitive overbuff profile (without heroes)! \n Usage: ``!competitive_short battletag#1234``");
                    EmbedFieldBuilder competitivemedium = new EmbedFieldBuilder();
                    competitivemedium.WithName("!competitive_medium");
                    competitivemedium.WithValue("Get the most important information from your competitive overbuff with little hero informatioin! \n Usage: ``!competitive_medium battletag#1234``");
                    EmbedFieldBuilder competitivefull = new EmbedFieldBuilder();
                    competitivefull.WithName("!competitive_long");
                    competitivefull.WithValue("Get your full competitive overbuff with little hero informatioin! \n Usage: ``!competitive_full battletag#1234``");*/




                    generalInfo.Fields.Add(registerautoupdate);
                    generalInfo.Fields.Add(unregisterautoupdate);
                    generalInfo.Fields.Add(activity);
                    generalInfo.Fields.Add(activitywiththeroes);
                    generalInfo.Fields.Add(competitive_hero);
                   /* generalInfo.Fields.Add(competitiveshort);
                    generalInfo.Fields.Add(competitivemedium);
                    generalInfo.Fields.Add(competitivefull);*/



                    await message.Channel.SendMessageAsync("", false, generalInfo.Build());

                    break;
                
                case "!register":

                    if (words[1].Length < 1 || words[1] == null)
                    {
                        await message.Channel.SendMessageAsync("You did not specify a battletag!");
                    }
                    else
                    {
                        string battleTag = words[1];
                        string urlReadyBattleTag = battleTag.Replace('#', '-');


                        ParseCompetitiveProfile parser = new ParseCompetitiveProfile();
                        CompetitiveProfile competitiveProfile = parser.ParseProfile(urlReadyBattleTag);

                        if (competitiveProfile.PublicProfile)
                        {
                            Embed generalInfo1 = GenerateRecentActivity(competitiveProfile).Build();
                            
                            await message.Channel.SendMessageAsync("", false, generalInfo1);

                            if (!BattleTags[((SocketGuildChannel)message.Channel).Guild.Id].ContainsKey(message.Channel.Id))
                            {
                                try
                                {
                                    BattleTagLastUpdate battleTagLast = new BattleTagLastUpdate();
                                    battleTagLast.tag = battleTag;
                                    List<BattleTagLastUpdate> tags = new List<BattleTagLastUpdate>();
                                    tags.Add(battleTagLast);
                                    BattleTags[((SocketGuildChannel)message.Channel).Guild.Id].Add(message.Channel.Id, tags);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.ToString());
                                }

                            }
                            else
                            {
                                try
                                {
                                    BattleTagLastUpdate battleTagLast = new BattleTagLastUpdate();
                                    battleTagLast.tag = battleTag;
                                    BattleTags[((SocketGuildChannel)message.Channel).Guild.Id][message.Channel.Id].Add(battleTagLast);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.ToString());
                                }
                            }

                            await message.Channel.SendMessageAsync("Registered your battletag! To unregister: ``!unregister " + battleTag + "``");
                        }
                        else
                        {
                            await message.Channel.SendMessageAsync("Could not register your battletag! Your profile is not Public!");
                        }
                       


                    }
                    break;
                case "!unregister":

                    if (words[1].Length < 1 || words[1] == null)
                    {
                        await message.Channel.SendMessageAsync("You did not specify a battletag!");
                    }
                    else
                    {
                        string battleTag = words[1];

                        if (BattleTags[((SocketGuildChannel)message.Channel).Guild.Id].ContainsKey(message.Channel.Id))
                        {
                            int index = -1;
                            foreach (BattleTagLastUpdate battleTagLastUpdate in BattleTags[((SocketGuildChannel)message.Channel).Guild.Id][message.Channel.Id])
                            {
                                if (battleTagLastUpdate.tag == battleTag)
                                {
                                    if (index == -1)
                                    {
                                        index = 0;
                                    }
                                    break;
                                }
                                index++;
                            }
                            if (index > -1)
                            {
                                BattleTags[((SocketGuildChannel)message.Channel).Guild.Id][message.Channel.Id].RemoveAt(index);

                                await message.Channel.SendMessageAsync("Succesfully Un-Registered your battletag! To register: ``!register " + battleTag + "``");
                            }
                            else
                            {

                                await message.Channel.SendMessageAsync("Could not Un-Registered your battletag! Does not exist (in channel).``");
                            }
                        }
                        else
                        {
                            await message.Channel.SendMessageAsync("Could not Un-Registered your battletag! Channel not registered.``");
                        }

                    }
                    break;
                case "!activity":

                    if (words[1].Length < 1 || words[1] == null)
                    {
                        await message.Channel.SendMessageAsync("You did not specify a battletag! Usage: ``!activity battletag#1234``");
                    }
                    else
                    {
                        string battleTag = words[1];
                        await message.Channel.SendMessageAsync("Retreiving profile for: " + battleTag);

                        string urlReadyBattleTag = battleTag.Replace('#', '-');
                        

                        ParseCompetitiveProfile parser = new ParseCompetitiveProfile();
                        CompetitiveProfile competitiveProfile = parser.ParseProfile(urlReadyBattleTag);

                        if (competitiveProfile.PublicProfile)
                        {
                            Embed generalInfo1 = GenerateRecentActivity(competitiveProfile).Build();

                            await message.Channel.SendMessageAsync("", false, generalInfo1);
                        }
                        else
                        {
                            await message.Channel.SendMessageAsync("Could not retreive your latest activity! Your profile is not Public!");
                        }

                    }
                    break;
                case "!activity_with_heroes":

                    if (words[1].Length < 1 || words[1] == null)
                    {
                        await message.Channel.SendMessageAsync("You did not specify a battletag!");
                    }
                    else
                    {
                        string battleTag = words[1];
                        await message.Channel.SendMessageAsync("Retreiving profile for: " + battleTag);

                        string urlReadyBattleTag = battleTag.Replace('#', '-');


                        ParseCompetitiveProfile parser = new ParseCompetitiveProfile();
                        CompetitiveProfile competitiveProfile = parser.ParseProfile(urlReadyBattleTag);
                        if (competitiveProfile.PublicProfile)
                        {
                            Embed generalInfo2 = GenerateRecentActivity(competitiveProfile).Build();

                            await message.Channel.SendMessageAsync("", false, generalInfo2);


                            foreach (KeyValuePair<string, string> hero1 in competitiveProfile.RecentActivity.First().Value.HeroesPlayed)
                            {

                                Console.WriteLine("Generating hero post: " + hero1.Key);
                                EmbedBuilder heropost = GenerateHeroPost(competitiveProfile, hero1.Key);
                                Console.WriteLine("Finished Generating hero post: " + hero1.Key);
                                if (heropost != null)
                                {
                                    try
                                    {
                                        Console.WriteLine("Posting hero post: " + heropost.Title);
                                        Embed embed = heropost.Build();
                                        await message.Channel.SendMessageAsync("", false, embed);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.ToString());
                                    }
                                }
                                else
                                {

                                    Console.WriteLine("Hero post returned NULL");
                                }
                            }
                        }
                        else
                        {
                            await message.Channel.SendMessageAsync("Could not retreive your latest activity! Your profile is not Public!");
                        }
                    }
                    break;
            }

        }

        private static void ParseCompetitiveProfile()
        {
            throw new NotImplementedException();
        }

        private static Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private static EmbedBuilder GenerateRecentActivity(CompetitiveProfile competitiveProfile)
        {
            EmbedBuilder generalInfo = new EmbedBuilder();

            EmbedAuthorBuilder authorBuilder = new EmbedAuthorBuilder();
            authorBuilder.Name = competitiveProfile.UserName;
            authorBuilder.IconUrl = competitiveProfile.UserIcon;

            generalInfo.WithAuthor(authorBuilder);
            generalInfo.WithTitle("Latest Activity: " + competitiveProfile.RecentActivity.First().Value.Type);
            generalInfo.WithDescription("Your latest acitivity on " + competitiveProfile.RecentActivity.First().Value.Date);
            generalInfo.ThumbnailUrl = competitiveProfile.UserIcon;

            if (competitiveProfile.RecentActivity.First().Value.Type != "Quick Play")
            {
                EmbedFieldBuilder skill = new EmbedFieldBuilder();
                skill.WithName("Skill Rating");
                skill.WithValue(competitiveProfile.RecentActivity.First().Value.SkillRating);
                skill.IsInline = true;
                generalInfo.Fields.Add(skill);
            }

            EmbedFieldBuilder won = new EmbedFieldBuilder();
            won.WithName("Games Won");
            won.WithValue(competitiveProfile.RecentActivity.First().Value.Won);
            won.IsInline = true;
            generalInfo.Fields.Add(won);

            EmbedFieldBuilder lost = new EmbedFieldBuilder();
            lost.WithName("Games Lost");
            lost.WithValue(competitiveProfile.RecentActivity.First().Value.Lost);
            lost.IsInline = true;
            generalInfo.Fields.Add(lost);


            EmbedFieldBuilder draw = new EmbedFieldBuilder();
            draw.WithName("Games Tied");
            draw.WithValue(competitiveProfile.RecentActivity.First().Value.Draw);
            draw.IsInline = true;
            generalInfo.Fields.Add(draw);

            EmbedFieldBuilder generalStats = new EmbedFieldBuilder();
            generalStats.WithName("Statistics");
            generalStats.WithValue("------------------");
            generalStats.IsInline = false;

            generalInfo.Fields.Add(generalStats);

            foreach (KeyValuePair<string, string> heroPrimaryStatistics in competitiveProfile.RecentActivity.First().Value.ActivityStats)
            {
                EmbedFieldBuilder statistics = new EmbedFieldBuilder();
                statistics.WithName(heroPrimaryStatistics.Key);
                statistics.WithValue(heroPrimaryStatistics.Value);
                statistics.IsInline = true;

                Console.WriteLine(heroPrimaryStatistics.Key + " : " + heroPrimaryStatistics.Value);

                generalInfo.Fields.Add(statistics);
            }

            return generalInfo;

        }

        private static EmbedBuilder GenerateHeroPost(CompetitiveProfile competitive, string heroname)
        {
            EmbedBuilder heroInfo = new EmbedBuilder();
            if (competitive.HeroesPlayed.ContainsKey(heroname)){
                heroInfo.WithTitle("Hero: " + heroname);
                heroInfo.WithAuthor(competitive.HeroesPlayed[heroname]["hero_info"]["hero_name"], competitive.HeroesPlayed[heroname]["hero_info"]["hero_icon"], competitive.HeroesPlayed[heroname]["hero_info"]["hero_url"]);
                heroInfo.WithUrl(competitive.HeroesPlayed[heroname]["hero_info"]["hero_url"]);
                heroInfo.ThumbnailUrl = competitive.HeroesPlayed[heroname]["hero_info"]["hero_icon"];
                EmbedFieldBuilder primaryfield = new EmbedFieldBuilder();
                primaryfield.WithName("Primary Statistics:");
                primaryfield.WithValue("------------------");
                primaryfield.IsInline = false;

                heroInfo.Fields.Add(primaryfield);

                foreach (KeyValuePair<string, string> heroPrimaryStatistics in competitive.HeroesPlayed[heroname]["primary_statistics"]) {
                    EmbedFieldBuilder statistics = new EmbedFieldBuilder();
                    statistics.WithName(heroPrimaryStatistics.Key);
                    statistics.WithValue(heroPrimaryStatistics.Value);
                    statistics.IsInline = true;

                    Console.WriteLine(heroPrimaryStatistics.Key + " : " + heroPrimaryStatistics.Value);

                  heroInfo.Fields.Add(statistics);
                }

                EmbedFieldBuilder secondaryfield = new EmbedFieldBuilder();
                secondaryfield.WithName("Secondary Statistics:");
                secondaryfield.WithValue("------------------");
                secondaryfield.IsInline = false;
                heroInfo.Fields.Add(secondaryfield);

                foreach (KeyValuePair<string, string> heroScondaryStatistics in competitive.HeroesPlayed[heroname]["secondary_statistics"])
                {
                    EmbedFieldBuilder statistics = new EmbedFieldBuilder();
                    statistics.WithName(heroScondaryStatistics.Key);
                    statistics.WithValue(heroScondaryStatistics.Value);
                    statistics.IsInline = true;
                    Console.WriteLine(heroScondaryStatistics.Key + " : " + heroScondaryStatistics.Value);
                    heroInfo.Fields.Add(statistics);
                }
                Console.WriteLine("Finsihed hero post for: " + competitive.HeroesPlayed[heroname]["hero_info"]["hero_name"]);     
            } 
            return heroInfo;
        }




    }
}
